/*
  Assignment:
    Read a string from the user

    If the string is "CAT", say "MEOW!"
    If the string is "DOG", say "WOOF!"
    If the string is not "CAT" or "DOG", say "NOISE!"

    IF AT ANY POINT YOUR CODE IS UNSAFE YOU'LL EAT A SANDWICH WITH NUTELLA AND CHICKEN
*/
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define BUFF_SIZE 50

int main(void)
{
  char buff[BUFF_SIZE];
  printf("Enter a string of characters: ");
  fgets(buff, sizeof(buff), stdin);

  size_t len = strlen(buff);

  // Check if the string is too long
  if (!(len <= BUFF_SIZE)) {
    printf("String too long, please enter a shorter string.\n");
    return 1;
  }

  // Check if the string is empty
  if (len == 1 && buff[0] == '\n') {
    printf("Empty string, please enter a non-empty string.\n");
    return 1;
  }

  if (buff[len - 1] == '\n')
    buff[len - 1] = '\0';

  // Convert user input to uppercase before comparison
  for (size_t i = 0; i < len; ++i) {
      buff[i] = (char)toupper((unsigned char)buff[i]);
  }

  // Compare strings
  if (strcmp(buff, "CAT") == 0) {
    printf("MEOW!\n");
  } else if (strcmp(buff, "DOG") == 0) {
    printf("WOOF!\n");
  } else {
    printf("NOISE!\n");
  }

  return 0;
}
